# MIUI Debloater

Script to remove various software from Microsoft/Google/Xiaomi/Facebook from Xiaomi/MIUI phones.  
Tested on a Global RedMi Note 5 Pro.  

This script requires `adb` in your `$path` and USB debugging enabled on your phone.  

Read the package list before using and decide by yourself what to remove!  
Removing some packages (like `com.xiaomi.finddevice`) might cause a bootloop. The bootloop can be fixed by a full device wipe, which will obviously delete all data from your phone.  

Note:
* remove `com.google.android.inputmethod.latin` only after you have installed an alternative keyboard. I suggest [Simple Keyboard].(https://f-droid.org/en/packages/rkr.simplekeyboard.inputmethod/)  
* removing `com.google.android.setupwizard` before the first setup might cause problems when activating the device. My suggestion is to first complete the setup, skip adding a Google account, and then removing it.  
* `com.google.android.gms` and `com.google.android.gsf` are the two main components of Play Services. Remove them only if you use applications which don't depend on them, because MicroG is not usable without root.  

Packages are removed with the `ADB` `pm uninstall -k --user 0` command, which only removes them for the current user. This allow deletion for non-root users and OTA upgrades, but the .apk are left in the /system partition.  
See [this article](https://www.xda-developers.com/uninstall-carrier-oem-bloatware-without-root-access/) on XDA for more informations.  