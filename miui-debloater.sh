#!/usr/bin/env bash

packages=(
	# Microsoft
	"com.skype.raider"
	"com.microsoft.office.excel"
	"com.microsoft.office.outlook"
	"com.microsoft.office.word"
	"com.microsoft.office.powerpoint"

	# Facebook
	"com.facebook.katana"
	"com.facebook.services"
	"com.facebook.appmanager"
	"com.facebook.system"

	# Google
	"com.google.android.gm" # GMail
	"com.google.android.youtube"
	"com.google.android.music"
	"com.google.android.googlequicksearchbox" # Google Widget
	"com.google.android.apps.tachyon" # Google Duo
	"com.google.android.apps.docs"
	"com.google.android.apps.maps"
	"com.google.android.syncadapters.contacts" # Sync contacts to Google
	"com.google.android.syncadapters.calendar" # Sync calendars to Google
	"com.google.android.videos"
	"com.google.android.partnersetup"
	"com.google.android.printservice.recommendation" # Probably related to Google Cloud Print
	"com.google.android.apps.photos"
	"com.google.android.feedback" # Feedback/Crash report
	"com.google.android.inputmethod.pinyin" # Google Chinese Keyboard
	"com.google.android.configupdater"
	"com.android.vending" # Google Play Store
	"com.android.chrome" # Google Chrome
	"com.google.android.setupwizard" # Google setup that runs at first boot
	"com.google.android.backuptransport"
	"com.google.android.onetimeinitializer"
	"com.google.android.inputmethod.latin" # Google Keyboard, install a different keyboard before uninstalling!
	"com.google.android.marvin.talkback" # Talkback, used for accessibility settings
	#"com.google.android.tts" # Text-To-Speach
	#"com.google.android.gms" # Play Services
	#"com.google.android.gsf" # Play Services
	#"com.google.android.ext.shared"
	#"com.google.android.ext.services"

	# Xiaomi
	"com.miui.analytics"
	"com.miui.cleanmaster"
	"com.miui.cloudservice" # Settings will crash when pressing on any "Mi Cloud" button
	"com.miui.cloudbackup"
	"com.xiaomi.micloud.sdk"
	"com.xiaomi.payment"
	"com.miui.klo.bugreport"
	"com.miui.bugreport"
	"com.miui.videoplayer"
	"com.xiaomi.midrop"
	"com.miui.virtualsim"
	"com.miui.vsimcore"
	"com.miui.yellowpage"
	"com.miui.daemon" # Doesn't seems to affect anything when uninstalled
	"com.android.browser" # Xiaomi Browser
	"com.miui.enbbs" # Xiaomi Forums
	"com.miui.weather2"
	#"com.miui.whetstone"
	#"com.xiaomi.finddevice" # Bootloop the device!
	"com.miui.msa.global" # MIUI System Ads Solution
	"com.miui.touchassistant" # Quick Ball/Touch Assistant
	"com.miui.screenrecorder"
	"com.xiaomi.discover"
	#"com.xiaomi.account"
	"com.xiaomi.mipicks"
	"com.miui.player"
	"com.miui.notes"
	"com.miui.gallery"
	"com.miui.translation.kingsoft"
	"com.miui.translation.youdao"
	"com.mi.android.globalFileexplorer"
	"com.miui.android.fashiongallery" # Wallpapers by xiaomi
	"com.mfashiongallery.emag"	# Wallpapers by xiaomi
	"com.mi.android.globalpersonalassistant" # MI Vault, aka the "assistent" you open swiping left from MI Home

	# Other
	"com.netflix.partner.activation"
	"com.dsi.ant.server" # Seems related to either FM radio or ANT bluetooth devices
)

if ! pgrep adb >/dev/null 2>&1; then
	printf 'Starting the ADB server\n'
	sudo adb start-server
fi

printf 'Waiting for the device...'
adb wait-for-device
printf ' Done!\n'

printf 'Getting installed packages...'
installed_packages=$(mktemp)
adb shell pm list packages | sed -e "s/^package://g" > "${installed_packages}"
printf ' Done!\n'

for package in "${packages[@]}"; do 
	if grep -q "^${package}$" "${installed_packages}"; then
		printf 'Uninstalling %s...' "${package}"
		# Uninstall the package only for the current user without removing the cache
		adb shell pm uninstall -k --user 0 "${package}" >/dev/null 2>&1
		printf ' Done!\n'
	fi
done

rm "${installed_packages}"
printf 'All Done!\n'
